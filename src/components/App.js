import React, {createContext, useRef, useState} from 'react';
import './App.css';

import {Controls} from "./Controls";
import {Preview} from "./Preview";
import {initialState as animationParams} from "./Controls/Animation";

export const initialState = {
    controls: [
        {controlName: 'margin-left', keyFrames: new Map([['from', 350], ['to', 0]])},
    ],
    keyFrameString: '',
    animationString: '',
    animationParams
}

export const AppContext = createContext()
const {Provider} = AppContext;

function App() {

    const initialValues = useRef(initialState)

    const [state, setState] = useState(initialValues.current)

        return (
            <Provider value={[state, setState]}>
            <div className="App">
                <h1 className="header">Make your animations</h1>
                <div className="content">
                    <Controls />
                    <Preview />
                </div>
            </div>
            </Provider>
        )
}
export default App;


