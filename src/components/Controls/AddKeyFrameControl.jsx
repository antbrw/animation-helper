import React from 'react'

export default function ({ addControl }) {
    const handleSubmit = e => {
        e.preventDefault()
        const { value } = e.target.controlName
        addControl({ controlName: value, keyFrames: new Map([ ['from', 50], ['to', 0]]) })
        e.target.controlName.value = ''
    }
    return (
        <form className="add-control" onSubmit={handleSubmit}>
            <label className="add-control__label" htmlFor="controlName">New parameter</label>
            <input placeholder="margin-left" className="add-control__input" name="controlName" type="text" />
            <button className="add-control__button">Add</button>
        </form>
    )
}
