import React, {useContext, useEffect} from 'react'
import {
    useGenerateActualStyles,
    useSetAnimationParams,
    useSetCssStrings
} from '../../hooks'
import {AppContext} from "../App";

export const initialState = {
    'animation-name': 'slide',
    'animation-duration': 1,
    'animation-timing-function': 'ease',
    'animation-delay': 0,
    'animation-iteration-count': 1,
    'animation-direction': 'normal',
    'animation-fill-mode': 'none',
    'animation-play-state': 'running',
}

export default function () {
    const generateStyleNodes = useGenerateActualStyles()
    const [{animationParams, animationString, keyFrameString}] = useContext(AppContext)
    const setAnimationParams = useSetAnimationParams()
    const setCssStrings = useSetCssStrings()
    const generateActualStyles = useGenerateActualStyles()

    useEffect(() => {
        generateActualStyles()
    }, [animationString, keyFrameString])

    const handleChange = (e) => {
        const {name, value} = e.target
        setAnimationParams(name, value)
    }
    useEffect(() => {
        setCssStrings()
        generateStyleNodes()
    }, [animationParams])

    return (
        <div className="animation">
            <form className="animation__form" onChange={handleChange}>
                <div className="animation__form__group">
                    <label htmlFor="animation-name">Name</label>
                    <input type="text" name="animation-name" value={animationParams['animation-name']}/>
                </div>
                <div className="animation__form__group">
                    <label htmlFor="animation-duration">Duration</label>
                    <input type="number" name="animation-duration" value={animationParams['animation-duration']}/>
                </div>
                <div className="animation__form__group">
                    <label htmlFor="animation-timing-function">Timing Function</label>
                    <select name="animation-timing-function" value={animationParams['animation-timing-function']}>
                        <option value="ease">ease</option>
                        <option value="ease-in">ease-in</option>
                        <option value="ease-out">ease-out</option>
                        <option value="ease-in-out">ease-in-out</option>
                        <option value="linear">linear</option>
                        <option value="step-start">step-start</option>
                        <option value="step-end">step-end</option>
                    </select>
                </div>
                <div className="animation__form__group">
                    <label htmlFor="animation-delay">Delay</label>
                    <input type="number" name="animation-delay" value={animationParams['animation-delay']}/>
                </div>
                <div className="animation__form__group">
                    <label htmlFor="animation-iteration-count">Iterations</label>
                    <input type="number" name="animation-iteration-count"
                           value={animationParams['animation-iteration-count']}/>
                </div>
                <div className="animation__form__group">
                    <label htmlFor="animation-direction">Direction</label>
                    <select name="animation-direction" value={animationParams['animation-direction']}>
                        <option value="normal">normal</option>
                        <option value="reverse">reverse</option>
                        <option value="alternate">alternate</option>
                        <option value="alternate-reverse">alternate-reverse</option>
                    </select>
                </div>
                <div className="animation__form__group">
                    <label htmlFor="animation-fill-mode">Fill mode</label>
                    <select name="animation-fill-mode" id="" value={animationParams['animation-fill-mode']}>
                        <option value="none">none</option>
                        <option value="forwards">forwards</option>
                        <option value="backwards">backwards</option>
                        <option value="both">both</option>
                    </select>
                </div>
                <div className="animation__form__group">
                    <label htmlFor="animation-play-state">Play state</label>
                    <select name="animation-play-state" id="" value={animationParams['animation-play-state']}>
                        <option value="running">running</option>
                        <option value="paused">paused</option>
                    </select>
                </div>
            </form>
        </div>
    )
}
