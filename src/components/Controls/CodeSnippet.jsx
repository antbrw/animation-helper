import React, {useContext} from 'react'
import {AppContext} from "../App";

const AMIMATION_COMMENT_LINE = '// be sure to change class name \n\n'
const ANIMATION_DEFAULT_CLASS = '.YOUR_CLASSNAME'

export default function () {
    const [{keyFrameString, animationString}] = useContext(AppContext)
    return (
        <div className="snippets">
            <p>Keyframes string</p>
            <textarea className="snippets__code-snippet" readOnly value={keyFrameString}/>
            <p>Animation string</p>
            <textarea className="snippets__code-snippet" readOnly value={`${AMIMATION_COMMENT_LINE} ${ANIMATION_DEFAULT_CLASS} ${animationString}`}/>
        </div>
    )
}
