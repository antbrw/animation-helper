import React from 'react'

export default function ({ control: { controlName, keyFrames }, onRemove, onChange, index }) {

    const handleRemove = () => onRemove(index)
    const handleChange = (e) => {
        const { name, value } = e.target
        onChange(index, name, value)
    }

    return (
        <div className="control">
            <span className="control--name">{controlName}</span>
            {Array.from(keyFrames).map(keyFrame => {
                const [name, value] = keyFrame
                const keyFrameId = `keyFrame-${name}`
                return (
                    <span className="control--keyframe" key={keyFrameId}>
                        <label className="control--keyframe--label" htmlFor={keyFrameId}>{name}</label>
                        <input className="input control--keyframe--range" min="0" max="999" type="range" name={name} value={value} onChange={handleChange} />
                        <input className="input control--keyframe--text" readOnly value={value} disabled />
                    </span>
                )
            })}
            <button className="control remove-button" onClick={handleRemove}>Delete</button>
        </div>
    )
}
