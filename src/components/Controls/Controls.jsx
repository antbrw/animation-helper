import React, {useEffect} from 'react'
import './Controls.css'
import Tabs from "../Tabs/Tabs";
import KeyFrames from "./KeyFrames";
import Animation from "./Animation";
import CodeSnippet from "./CodeSnippet";
import {useGenerateActualStyles} from "../../hooks";

function Controls() {

    return (
            <div className="controls">
                <Tabs tabs={['Key Frames', 'Animation', 'Code Snippet']}>
                    <Tabs.Tab name='Key Frames'>
                        <KeyFrames/>
                    </Tabs.Tab>
                    <Tabs.Tab name='Animation'>
                        <Animation/>
                    </Tabs.Tab>
                    <Tabs.Tab name='Code Snippet'>
                        <CodeSnippet/>
                    </Tabs.Tab>
                </Tabs>
            </div>
    )
}

export default Controls;
