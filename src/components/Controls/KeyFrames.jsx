import React, {useEffect, useContext} from 'react'
import {
    useAddControl,
    useRemoveControl,
    useChangeKeyFrame,
    useGenerateActualStyles,
    useSetCssStrings,
} from "../../hooks";
import Control from "./Control";
import AddControl from "./AddKeyFrameControl";
import {AppContext} from "../App";

export default function () {
    const [{ controls, animationParams, keyFrameString, animationString }] = useContext(AppContext)
    const setCssStrings = useSetCssStrings()
    const generateActualStyles = useGenerateActualStyles()

    useEffect(() => {
        generateActualStyles()
    }, [animationString, keyFrameString])

    useEffect(() => {
        setCssStrings()
    }, [controls, animationParams])

    const handleAdd = useAddControl()
    const handleRemove = useRemoveControl()
    const handleChange = useChangeKeyFrame()

    return (
        <>
            <AddControl addControl={handleAdd}/>
            {controls.map((control, index) => <Control key={`${control.controlName}-${index}`} control={control}
                                                       onChange={handleChange} onRemove={handleRemove} index={index} />)}
        </>
    )
}
