import React, {useState, useContext, useEffect} from 'react'
import './Preview.css'
import Model from "./Model";
import {AppContext} from "../App";

export default function () {
    const [reload, setReload] = useState(true)

    const [{ animationParams, controls }] = useContext(AppContext)

    useEffect(() => {
        update()
    }, [animationParams, controls])

    const update = () => {
        setReload(false)
        setTimeout(() => setReload(true), 1)
    }
    const handleClick = () => {
        update()
    }
    return (
        <div>
            <div className="preview" id="preview">
                {reload && <Model/>}
            </div>
            <button className="preview__button" onClick={handleClick}>Update</button>
        </div>
    )
}
