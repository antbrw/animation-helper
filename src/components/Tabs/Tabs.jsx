import React, {useState} from 'react';
import cx from 'clsx'
import './Tabs.css'

const Tabs = ({tabs = [], children}) => {

    if (!tabs[0]) {
        throw new Error('Must provide tab names')
    }

    const [active, setActive] = useState(tabs[0])

    const handleTabNameClick = (e) => {
        setActive(e.target.dataset.name)
    }

    return (
        <div className="tabs">
            <div className="tabs__header">{tabs.map((tab, index) => <span key={`${tab}-${index}`} data-name={tab}
                                                                          className={cx('tabs__header__tab', active === tab && 'active')}
                                                                          onClick={handleTabNameClick}>{tab}</span>)}</div>
            {React.Children.map(children, child => {
                if (child.props.name === active) {
                    return React.cloneElement(child, null)
                }
            })}
        </div>
    )
}

const Tab = ({children}) => {
    return <>{children}</>
}

Tabs.Tab = Tab

export default Tabs
