const MODEL_CLASSNAME = '.preview__model'

export default function (styleName = '', styleString = '') {
    switch (styleName) {
        case 'animationString':
            // add class name to string, so it actually affects preview
            return `${MODEL_CLASSNAME} ${styleString}`
        default:
            return styleString
    }
}
