import {useCallback, useContext, useEffect, useRef} from 'react'
import produce, {enableMapSet} from "immer";
import {AppContext} from "../components/App";
import useStyleRef from "./useStyleRef";
import {generateStyleHelper} from "../helpers";
import useProcessKeyFrames from "./useProcessKeyFrames";

// allow immer to Map.set()
enableMapSet()

export const useAddControl = () => {
    const [state, setState] = useContext(AppContext)
    return control => {
        if (state.controls.find(candidate => candidate.controlName ===control.controlName)) {
            return null
        }
        return setState(produce(state, draft => {
            draft.controls.push(control)
        }))
    }
}
export const useRemoveControl = () => {
    const [state, setState] = useContext(AppContext)
    return index => setState(produce(state, draft => {
        draft.controls = draft.controls.filter((c, i) => i !== index)
    }))
}

export const useChangeKeyFrame = () => {
    const [state, setState] = useContext(AppContext)
    return (controlIndex, keyFrameName, value) => setState(produce(state, draft => {
        draft.controls[controlIndex].keyFrames.set(keyFrameName, value)

    }))
}

export const useAddKeyFrame = () => {
}

export const useRemoveKeyFrame = () => {
}

/*
    set animation parameter action
 */
export const useSetAnimationParams = () => {
    const [state, setState] = useContext(AppContext)
    return (name, value) => setState(produce(state, draft => {
        draft.animationParams[name] = value
    }))
}

/*
    generate animation css string
 */
export const useStringifyAnimationParams = () => {
    return (animationParams) => {
        const data = Object.keys(animationParams).map(param => {
            let value = animationParams[param]
            switch (param) {
                case 'animation-duration':
                    value = `${value}s`
                    break
                case 'animation-delay':
                    value = `${value}ms`
                    break
                default:
                    break
            }
            return `    ${param}: ${value};`
        })
        return `{\n${data.join('\n')}\n}`
    }
}

/*
    process control parameters & generate keyframe css string
 */
export const useStringifyControls = () => {
    return (keyFrameData, animationParams) => {
        const data = Object.keys(keyFrameData).map(kf => {
            const keyFrame = keyFrameData[kf]
            const singleKeyFrameData = Object.keys(keyFrame).map(controlName => `       ${controlName}: ${keyFrame[controlName]};\n`).join('')
            return `    ${kf} {\n${singleKeyFrameData}    }`
        }).join('\n')

        if (!data) {
            return ''
        }

        return `@keyframes ${animationParams['animation-name']} {\n${data} \n}`
    }
}
/*
    set generated css strings to state
 */
export const useSetCssStrings = () => {
    const [state, setState] = useContext(AppContext)
    const processKeyFrames = useProcessKeyFrames()
    const stringifyControls = useStringifyControls()
    const stringifyAnimationParams = useStringifyAnimationParams()
    return () => setState(produce(state, draft => {
        draft.keyFrameString = stringifyControls(processKeyFrames(state.controls), state.animationParams)
        draft.animationString = stringifyAnimationParams(state.animationParams)
    }))
}
export const useGenerateActualStyles = () => {
    const [state] = useContext(AppContext)
    const keyFrameStylesRef = useStyleRef('keyFrameString'), animationStylesRef = useStyleRef('animationString')

    return () => {
        [keyFrameStylesRef, animationStylesRef].forEach(ref => {
            ref.current.childNodes.forEach(child => ref.current.removeChild(child))
            const textNode = document.createTextNode(generateStyleHelper(ref.current.id, state[ref.current.id]))
            ref.current.appendChild(textNode)
        })
    }
}


export default {
    useAddControl,
    useRemoveControl,
    useAddKeyFrame,
    useRemoveKeyFrame
}
