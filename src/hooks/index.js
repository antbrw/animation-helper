export {
    default as controlsHooks,
    useAddControl,
    useRemoveControl,
    useAddKeyFrame,
    useRemoveKeyFrame,
    useSetAnimationParams,
    useChangeKeyFrame,
    useGenerateActualStyles,
    useSetCssStrings
} from './controlsHooks'
