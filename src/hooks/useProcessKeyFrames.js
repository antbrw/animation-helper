export default function () {
    const processKeyFrames = (controls) => {
        if (controls.length < 1) {
            return {}
        }

        const newKeyFrameData = {}

        controls
            .forEach(control => {
                const {controlName, keyFrames} = control
                keyFrames
                    .forEach((value, name) => {
                            value = `${value}%`
                            newKeyFrameData[name] = {
                                ...newKeyFrameData[name],
                                [controlName]: value
                            }
                        }
                    )
            })
        return newKeyFrameData
    }

    return processKeyFrames
}
