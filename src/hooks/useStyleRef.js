import {useRef, useEffect} from 'react'

export default function (id = 'css') {
    const styleRef = useRef()
    useEffect(() => {
        styleRef.current = document.getElementById(id) || document.createElement('style')
        styleRef.current.type = 'text/css'
        styleRef.current.id = id
        document.head.appendChild(styleRef.current)
    }, [id])

    return styleRef
}
